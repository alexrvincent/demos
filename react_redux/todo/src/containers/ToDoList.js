
/* Import needed dependencies to run this component*/
import React from 'react'
import TodoButton from '../components/TodoButton'

/* Get your action creator and redux's special connect function to
   link the Redux/store to this container
 */
import { connect } from 'react-redux'
import { addTodo, removeTodo } from "../actions/index";

export class ToDoList extends React.Component {

    constructor(props){
        super(props);
        this.state = { "message": "No message"}
    }

    componentDidMount(){
        document.addEventListener("message", (event) => {
                this.setState({message: JSON.stringify(event.data)})
        });
    }

    render(){
        let { addTodo, removeTodo, state } = this.props;

        return (
            <div>
                {/*<p>Todos: {state.todo.todos}</p>*/}
                {/*<TodoButton label="+ Add" onClick={addTodo}/>*/}
                {/*<TodoButton label="- Remove" onClick={removeTodo}/>*/}

                <p> (This is the WebView!) </p>

                <p> Message from React Native: {this.state.message}</p>

                <TodoButton label=" Send a message to React Native"
                            onClick={ () => {window.postMessage("Hello from the Web!")}}/>
                            {/*onClick={ () => {window.postMessage(JSON.stringify({"message": "Hello from the Web!"}))}}/>*/}


            </div>
        )
    }
}

/* A special redux function (we actually define) that maps the linkages between the store
  and your container. This linkage is done over the props value.
 */
const mapStateToProps = (state, props) => {
    return {
        state
    }
};


/* A special redux function (we actually define) that maps the linkages between the desired dispatches (action
  creators) and your container. This linkage is done over the props value also.
 */
const mapDispatchToProps = (dispatch, props) => {
    return {
        addTodo: () => {
            dispatch(addTodo("Add this todo!"))
        },
        removeTodo: () => {
            dispatch(removeTodo("Remove this todo"))
        }
    }
};


/* A special redux function you need to link your container to the component itself.
 */
export default connect(mapStateToProps, mapDispatchToProps)(ToDoList)