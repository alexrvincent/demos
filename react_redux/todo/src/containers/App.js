/* Import your base dependencies */
import React, { Component } from 'react';
import '../themes/App.css';

/* Import your containers */
import ToDoList from '../containers/ToDoList'

/* These items are required to get redux working with react */
import { Provider } from 'react-redux';
import { createStore } from 'redux';

/* Get your synthesized reducers and create the store with it
 * Create store takes a few params:
 * 1) Your combined reducers
 * 2) Your store's initial state
 * 3) List of enhancers/middlewares
 * */
import todoApp from '../reducers'
let store = createStore(todoApp, { todo: {todos: 0} }, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <ToDoList/>
        </Provider>
    );
  }
}

export default App;
