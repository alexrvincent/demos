import React from 'react'
import PropTypes from 'prop-types'


export class TodoButton extends React.Component {

    render() {

        let { onClick, label } = this.props;

        return (
            <button onClick={onClick}> {label}</button>
        )
    }
}

TodoButton.propTypes = {
    onClick: PropTypes.func,
    label: PropTypes.string
};

export default TodoButton;