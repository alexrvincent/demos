/* Note we use Object.assign to make a new version of the state and any changes we need to make to that state.
*
* In this case:
*
* var newState = (empty object) + (old state) + (changes to old state)
*
* */


const todos = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO':
            console.log("Adding a todo.");
            return Object.assign({}, state, { todos: state.todos + 1 });
        case 'DELETE_TODO':
            console.log("Removing a todo.");
            return Object.assign({}, state, { todos: state.todos > 0 ? state.todos - 1: 0});
        default:
            return state
    }
}

export default todos