from django.apps import AppConfig


class MessagingDemoAppConfig(AppConfig):
    name = 'messaging_demo_app'
